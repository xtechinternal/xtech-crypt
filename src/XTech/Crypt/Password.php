<?php
/**
 *
 * @author Andrew Chinn <andrew.chinn@xtechhq.com>
 * @author https://defuse.ca
 * Test Vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
 *
 */
	namespace XTech\Crypt;

	class Password
	{

		/**
		 * This method will generate encrypt a password and return the password hash and the salt.  This password generator, generates a 64 byte hash and 64 byte salt.
		 * @param String $password
		 * @param string $raw_output
		 * @return \stdClass
		 */
		static function encrypt($password, $salt = null, $raw_output = false)
		{
			$key_length = 64;
			$count = 16384;
			$algorithm = 'sha256';
			//If no salt is supplied then we will generate our own.
			if($salt ===  null)
			{
				$salt = openssl_random_pseudo_bytes($key_length);
			}

			$response = new \stdClass();

			$response->salt = $salt;

			if(!in_array($algorithm, hash_algos(), true))
				trigger_error('PBKDF2 ERROR: Invalid hash algorithm.', E_USER_ERROR);
			if($count <= 0 || $key_length <= 0)
				trigger_error('PBKDF2 ERROR: Invalid parameters.', E_USER_ERROR);

			if (function_exists("hash_pbkdf2")) {
				// The output length is in NIBBLES (4-bits) if $raw_output is false!
				if (!$raw_output) {
					$key_length = $key_length * 2;
				}
				$response->hash = hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
				return $response;
			}

			$hash_length = strlen(hash($algorithm, "", true));
			$block_count = ceil($key_length / $hash_length);

			$output = "";
			for($i = 1; $i <= $block_count; $i++) {
				// $i encoded as 4 bytes, big endian.
				$last = $salt . pack("N", $i);
				// first iteration
				$last = $xorsum = hash_hmac($algorithm, $last, $password, true);
				// perform the other $count - 1 iterations
				for ($j = 1; $j < $count; $j++) {
					$xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
				}
				$output .= $xorsum;
			}

			if($raw_output)
			{
				$response->hash = substr($output, 0, $key_length);
			}
			else
			{
				$response->hash = bin2hex(substr($output, 0, $key_length));
			}
			return $response;
		}
	}
